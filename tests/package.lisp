(in-package :cl-user)

(defpackage :tcr.sequence-iterators.test
    (:use :common-lisp :tcr.sequence-iterators)
    (:export #:signals-condition-p
             #:2+ #:2-
             #:type=
             #:sequence-type-of
             #:sequence-element-type
             #:*sequence* #:*size* #:*start* #:*end* #:*from-end*
             #:define-sequence-test))