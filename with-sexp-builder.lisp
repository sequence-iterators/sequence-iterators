(in-package :tcr.sequence-iterators)

;;; WITH-SEXP-BUILDER
;;;
;;; This is a macro-writing helping macro to write macros in a way
;;; that resembles the way you insert text into buffers in Elisp.
;;;
;;; WITH-SEXP-BUILDER maintains a pointer into the sexp-structure
;;; you're currently constructing. The pointer determines where new
;;; stuff is going to be inserted.
;;;
;;; Inside of WITH-SEXP-BUILDER, you can use the following locally
;;; bound functions:
;;;
;;;    (INSERT SEXP) - Inserts SEXP at the place the internal pointer
;;;                    points to; then moves the pointer to point
;;;                    behind SEXP. Like `insert' in Elisp.
;;;
;;;    (NEST)        - Moves the pointer into the last cons cell of the most
;;;                    recently inserted sexp.
;;;                    Like `(down-list -1)' in Elisp.
;;;
;;;    (NEST SEXP)   - Abbreviation for (PROGN (INSERT SEXP) (NEST)),
;;;                    i.e. first inserts SEXP, then moves the pointer
;;;                    to the last cell in SEXP.
;;;                    Like `(progn (insert sexp) (down-list -1))' in Elisp.
;;;
;;; The argument (START-SYMBOL . STUFF) of WITH-SEXP-BUILDER specifies
;;; the initial sexp that all invocations of INSERT/NEST are relative
;;; to. That is, the pointer is initially set to the last cell of that
;;; sexp.
;;;
;;; Examples:
;;;
;;;    (with-sexp-builder (progn)              (progn (alpha 1 2 3)
;;;      (insert '(alpha 1 2 3))          ==>         (beta 4 5 6)
;;;      (insert '(beta 4 5 6))                       (gamma 7 8 9))
;;;      (insert '(gamma 7 8 9)))
;;;
;;;
;;;    (with-sexp-builder (let ((x 42)))  ==>  (let ((x 42))
;;;      (insert '(+ x 1)))                      (+ x 1))
;;;
;;;
;;;    (with-sexp-builder (progn)               (progn
;;;      (insert '(let ((x 1))))                  (let ((x 1))
;;;      (nest)                           ==>       (let ((y 2)) 
;;;      (insert '(let ((y 2))))                      (setf x (+ x y))
;;;      (nest)                                       (+ x y))))
;;;      (insert '(setf x (+ x y)))
;;;      (insert '(+ x y)))
;;;
;;;
;;;    (with-sexp-builder (progn)               (progn
;;;      (insert '(let ((x 1))))                  (let ((x 1))
;;;      (nest   '(let ((y 2))))          ==>       (let ((y 2))
;;;      (nest   '(+ x y)))                           (+ x y))))
;;;
(defmacro with-sexp-builder ((start-symbol &rest stuff) &body body)
  ;;; FIXME: Think about API, desired features; then export it.
  (let ((result     (gensym "RESULT+"))
	(nest-ptr   (gensym "NEST-PTR+"))
	(insert-ptr (gensym "INSERT-PTR+"))
	(args (cons start-symbol stuff)))
    `(let* ((,result      (copy-list ',args))
	    (,nest-ptr    (last ,result))
	    (,insert-ptr  (last ,result)))
       (flet ((nest (&optional (thing nil thingp))
		(if (not thingp)
		    (setf ,insert-ptr ,nest-ptr)
		    (let ((thing (copy-list thing)))
		      (setf (cdr ,nest-ptr) (list thing))
		      (setf ,insert-ptr (cdr ,nest-ptr))
		      (setf ,nest-ptr (last thing))
		  )))
	      (insert (thing)
		(if (atom thing)
		    (progn (setf (cdr ,insert-ptr) (list thing))
			   (setf ,insert-ptr (cdr ,insert-ptr))
			   (setf ,nest-ptr ,insert-ptr))
		    (let ((thing (copy-list thing)))
		      (setf (cdr ,insert-ptr) (list thing))
		      (setf ,insert-ptr (cdr ,insert-ptr))
		      (setf ,nest-ptr (last thing))
		      ))))
	 (declare (ignorable #'nest #'insert))
	 ,@body
	 ,result))))
