;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :cl-user)

(asdf:defsystem :extensible-sequences
  :description "Extensible Sequences for CL"
  :author "Adlai Chandrasekhar <munchking@gmail.com>"
  :version "1.0 (unpublished so far)"
  :licence "BSD"
  :depends-on (:sequence-iterators)
  :components
  ((:file "package")
   (:file "cl-sequence-functions"   :depends-on ("package"))
   (:file "more-sequence-functions" :depends-on ("package"))))

(defmethod asdf:perform ((o asdf:test-op)
                         (c (eql (asdf:find-system :extensible-sequences))))
  (asdf:operate 'asdf:load-op :extensible-sequences-test)
  (asdf:operate 'asdf:test-op :extensible-sequences-test))

(asdf:defsystem :extensible-sequences-test
  :description "Test suite for the Extensible-Sequences library."
  :author "Adlai Chandrasekhar <munchking@gmail.com>"
  :depends-on (:sequence-iterators-test :extensible-sequences)
  :components
  ((:module tests
    :components
    ((:file "package")
     (:file "rt"             :depends-on ("package"))
     (:file "universe"       :depends-on ("package"))
     (:file "infrastructure" :depends-on ("rt" "universe"))
     (:file "length"         :depends-on ("infrastructure"))
     (:file "more"           :depends-on ("infrastructure"))))))

(defmethod asdf:perform ((o asdf:test-op)
                         (c (eql (asdf:find-system :extensible-sequences-test))))
  (let ((*package* (find-package :tcr.extensible-sequences.test)))
    (time (funcall (intern (string '#:do-tests) *package*)))))
