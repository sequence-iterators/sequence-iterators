;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :cl-user)

#. (let ((cl-sequence-functions
          '(#:concatenate #:copy-seq #:count #:count-if #:count-if-not
            #:delete #:delete-duplicates #:delete-if #:delete-if-not
            #:elt
            #:fill #:find #:find-if #:find-if-not
            #:length
            #:make-sequence #:map #:map-into #:merge #:mismatch
            #:nreverse #:nsubstitute #:nsubstitute-if #:nsubstitute-if-not
            #:position #:position-if #:position-if-not
            #:reduce #:remove #:remove-duplicates #:remove-if #:remove-if-not #:replace #:reverse
            #:search #:sort #:stable-sort #:subseq #:substitute #:substitute-if #:substitute-if-not))
         (more-sequence-functions
          '(#:map-into-subseq
            #:split-sequence
            #:take-while
            #:drop-while))
         (cl-symbols))
     (do-external-symbols (s :cl cl-symbols)
       (push s cl-symbols))
     
     `(progn
        (defpackage :tcr.extensible-sequences
          (:use :common-lisp :tcr.sequence-iterators)
          (:import-from :tcr.sequence-iterators #:define-api #:ensure-function)
          (:shadow ,@cl-sequence-functions)
          (:export ,@cl-sequence-functions
                   ,@more-sequence-functions))

        (defpackage :tcr.extensible-sequences-user
          (:use :common-lisp
                :tcr.sequence-iterators
                :tcr.extensible-sequences)
          (:shadowing-import-from :tcr.extensible-sequences
                                  ,@cl-sequence-functions)
          (:export ,@cl-symbols
                   ,@more-sequence-functions))))
