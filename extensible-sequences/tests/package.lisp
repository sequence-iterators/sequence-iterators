;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :cl-user)

(defpackage :tcr.extensible-sequences.test
    (:use :tcr.extensible-sequences-user
          :tcr.sequence-iterators
          :tcr.sequence-iterators.test))
