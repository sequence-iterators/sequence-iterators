;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

;;;; Some of these are adapted from the ANSI test suite.

(in-package :tcr.extensible-sequences.test)

(defmacro with-gensyms (names &body body)
  `(let ,(loop for name in names collect `(,name (gensym ,(symbol-name name))))
     ,@body))

(defun make-special-string (string &key fill adjust displace base)
  (let* ((len (length string))
         (len2 (if fill (+ len 4) len))
         (etype (if base 'base-char 'character)))
    (if displace
        (let ((s0 (make-array (+ len2 5)
                              :initial-contents
                              (concatenate 'string
                                           (make-string 2 :initial-element #\X)
                                           string
                                           (make-string (if fill 7 3)
                                                        :initial-element #\Y))
                              :element-type etype)))
          (make-array len2 :element-type etype
                      :adjustable adjust
                      :fill-pointer (if fill len nil)
                      :displaced-to s0
                      :displaced-index-offset 2))
        (make-array len2 :element-type etype
                    :initial-contents
                    (if fill (concatenate 'string string "ZZZZ") string)
                    :fill-pointer (if fill len nil)
                    :adjustable adjust))))

(defmacro do-special-strings ((var string-form &optional ret-form) &body forms)
  (with-gensyms (string fill adjust base displace)
    `(let ((,string ,string-form))
       (dolist (,fill '(nil t) ,ret-form)
         (dolist (,adjust '(nil t))
           (dolist (,base '(nil t))
             (dolist (,displace '(nil t))
               (let ((,var (make-special-string ,string :fill ,fill :adjust ,adjust
                                                :base ,base :displace ,displace)))
                 ,@forms))))))))

(defun printable-p (obj)
  "Returns T if obj can be printed to a string."
  (with-standard-io-syntax
   (let ((*print-readably* nil)
         (*print-escape* nil))
     (declare (optimize safety))
     (handler-case (and (stringp (write-to-string obj)) t)
                   (condition (c) (declare (ignore c)) nil)))))

(defmacro signals-type-error (var datum-form form &key (safety 3) (inline nil))
  (let ((lambda-form
         `(lambda (,var)
            (declare (optimize (safety ,safety)))
            ,form)))
    `(let ((,var ,datum-form))
       (declare (optimize safety))
       (handler-bind
           ((warning 'muffle-warning))
         (handler-case
             (apply #'values nil
                    (multiple-value-list
                     (funcall ,(cond (inline `(function ,lambda-form))
                                     (*compile-tests* `(compile nil ',lambda-form))
                                     (t `(eval ',lambda-form)))
                              ,var)))
           (type-error (c)
             (let ((datum (type-error-datum c))
                   (expected-type (type-error-expected-type c)))
               (cond ((not (eql ,var datum))
                      (list :datum-mismatch ,var datum))
                     ((typep datum expected-type)
                      (list :is-typep datum expected-type))
                     (t (printable-p c))))))))))

(declaim (special *mini-universe*))

(defun check-type-error* (pred-fn guard-fn &optional (universe *mini-universe*))
  "Check that for all elements in some set, either guard-fn is true or
   pred-fn signals a type error."
  (let (val)
    (loop for e in universe
          unless (or (funcall guard-fn e)
                     (equal
                      (setf val (multiple-value-list
                                 (signals-type-error x e (funcall pred-fn x) :inline t)))
                      '(t)))
        collect (list e val))))

(defmacro check-type-error (&body args)
  `(locally (declare (optimize safety)) (check-type-error* ,@args)))

(defmacro signals-error (form error-name &key (safety 3) (name nil name-p) (inline nil))
  `(handler-bind
       ((warning #'(lambda (c) (declare (ignore c))
                           (muffle-warning))))
     (proclaim '(optimize (safety 3)))
     (handler-case
         (apply #'values
                nil
                (multiple-value-list
                 ,(cond (inline form)
                        (*compile-tests*
                         `(funcall (compile nil '(lambda ()
                                                  (declare (optimize (safety ,safety)))
                                                  ,form))))
                        (t `(eval ',form)))))
       (,error-name (c)
         (cond
           ,@(case error-name
                   (type-error
                    `(((typep (type-error-datum c)
                              (type-error-expected-type c))
                       (values
                        nil
                        (list (list 'typep (list 'quote
                                                 (type-error-datum c))
                                    (list 'quote
                                          (type-error-expected-type c)))
                              "==> true")))))
                   ((undefined-function unbound-variable)
                    (and name-p
                         `(((not (eq (cell-error-name c) ',name))
                            (values
                             nil
                             (list 'cell-error-name "==>"
                                   (cell-error-name c)))))))
                   ((stream-error end-of-file reader-error)
                    `(((not (streamp (stream-error-stream c)))
                       (values
                        nil
                        (list 'stream-error-stream "==>"
                              (stream-error-stream c))))))
                   (file-error
                    `(((not (pathnamep (pathname (file-error-pathname c))))
                       (values
                        nil
                        (list 'file-error-pathname "==>"
                              (file-error-pathname c))))))
                   (t nil))
           (t (printable-p c)))))))

(defun make-special-integer-vector (contents &key fill adjust displace (etype 'integer))
  (let* ((len (length contents))
         (min (reduce #'min contents))
         (max (reduce #'max contents))
         (len2 (if fill (+ len 4) len)))
    (unless (and (typep min etype)
                 (typep max etype))
      (setq etype `(integer ,min ,max)))
    (if displace
        (let ((s0 (make-array (+ len2 5)
                              :initial-contents
                              (concatenate 'list
                                           (make-list 2 :initial-element
                                                      (if (typep 0 etype) 0 min))
                                           contents
                                           (make-list (if fill 7 3)
                                                      :initial-element
                                                      (if (typep 1 etype) 1 max)))
                              :element-type etype)))
          (make-array len2 :element-type etype
                      :adjustable adjust
                      :fill-pointer (if fill len nil)
                      :displaced-to s0
                      :displaced-index-offset 2))
      (make-array len2 :element-type etype
                  :initial-contents
                  (if fill (concatenate 'list
                                        contents
                                        (make-list 4 :initial-element
                                                   (if (typep 2 etype) 2 (floor (+ min max) 2))))
                    contents)
                  :fill-pointer (if fill len nil)
                  :adjustable adjust))))

(defmacro do-special-integer-vectors ((var vec-form &optional ret-form) &body forms)
  (with-gensyms (vector fill adjust etype displace)
    `(let ((,vector ,vec-form))
       (dolist (,fill '(nil t) ,ret-form)
         (dolist (,adjust '(nil t))
           (dolist (,etype ',(append (loop for i from 1 to 32 collect `(unsigned-byte ,i))
                                     (loop for i from 2 to 32 collect `(signed-byte ,i))
                                     '(integer)))
             (dolist (,displace '(nil t))
               (let ((,var (make-special-integer-vector
                            ,vector
                            :fill ,fill :adjust ,adjust
                            :etype ,etype :displace ,displace)))
                 ,@forms))))))))
