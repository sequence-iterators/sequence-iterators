;;; -* Mode:Lisp -*

(in-package :cl-user)

(asdf:defsystem :sequence-iterators
  :description "DOSEQUENCE & Co."
  :author "Tobias C. Rittweiler <trittweiler@common-lisp.net>"
  :version "(unpublished)"
  :licence "BSD"
  :depends-on (:parse-declarations-1.0)
  :components
  ((:file "package")
   (:file "utils"              :depends-on ("package"))
   (:file "with-sexp-builder"  :depends-on ("package"))
   (:file "define-api"         :depends-on ("package"))
   (:file "sequence-iterators" :depends-on ("package"
                                            "utils"
                                            "define-api"
                                            "with-sexp-builder"))))

(defmethod asdf:perform ((o asdf:test-op)
                         (c (eql (asdf:find-system :sequence-iterators))))
  (asdf:operate 'asdf:load-op :sequence-iterators-test)
  (asdf:operate 'asdf:test-op :sequence-iterators-test))

(asdf:defsystem :sequence-iterators-test
  :description "Test suite for the Sequence-Iterators library."
  :author "Tobias C. Rittweiler <trittweiler@common-lisp.net>"
  :depends-on (:sequence-iterators)
  :components
  ((:module tests
    :components
    ((:file "package")
     (:file "rt"             :depends-on ("package"))
     (:file "infrastructure" :depends-on ("package" "rt"))
     (:file "tests"          :depends-on ("package" "infrastructure"))))))

(defmethod asdf:perform ((o asdf:test-op)
                         (c (eql (asdf:find-system
                                  :sequence-iterators-test))))
  (let ((*package* (find-package :tcr.sequence-iterators.test)))
    (time (funcall (intern (string '#:do-tests) *package*)))))

;; (documentation-template:create-template
;;  :sequence-iterators
;;  :target "/home/tcr/src/contributing/sequence-iterators/doc/sequence-iterators.html"
;;  :version (asdf:component-version (asdf:find-system :sequence-iterators))
;;  :authors '("Tobias C Rittweiler" "Adlai Chandrasekhar")
;;  :repository "http://common-lisp.net/project/sequence-iterators/darcs/sequence-iterators/"
;;  :download-url "does-not-exist")
