(in-package :tcr.sequence-iterators)

(deftype sequence-index (&optional (length '* lengthp))
  "Represents an index into a sequence.

If `length' is explicitly given, it's taken to mean the (exclusive)
upper bound of the sequence.

Otherwise, it's either a non-negative integer in the general case, or
a non-negative fixnum on implementations where such an assumption is
reasonable."
  (if lengthp
      `(integer 0 (,length))
      `(integer 0
                #+ (or sbcl ccl) (,most-positive-fixnum)
                #- (or sbcl ccl) *)))

(deftype sequence-length ()
  "Represents the length of a sequence.

As this type is equivalent to SEQUENCE-INDEX + 1, you have to
carefully consider whether something represents an index or the length
of a sequence (lest you're bitten by an off-by-one bug.) For example,
the :START parameter is of type SEQUENCE-INDEX, but the :END parameter
is of type SEQUENCE-LENGTH."
  `(integer 0
            #+ (or sbcl ccl) ,most-positive-fixnum
            #- (or sbcl ccl) *))

(define-condition sequence-out-of-bounds-error (type-error) ())

(define-condition sequence-bounding-indices-error (sequence-out-of-bounds-error)
  ((sequence
    :initarg :sequence
    :initform (required-argument)
    :reader sequence-out-of-bounds-error--sequence
    :type sequence)
   (start
    :initarg :start
    :initform (required-argument)
    :reader sequence-out-of-bounds-error--start
    :type sequence-index)
   (end
    :initarg :end
    :initform (required-argument)
    :reader sequence-out-of-bounds-error--end
    :type sequence-length))
  (:report
   (lambda (condition stream)
     (format stream "~@<The bounding indices ~D and ~D do not fit to ~
                        ~:_a sequence of length ~D: ~
                          ~4I~:_~S~:>"
             (sequence-out-of-bounds-error--start condition)
             (sequence-out-of-bounds-error--end condition)
             (length (sequence-out-of-bounds-error--sequence condition))
             (sequence-out-of-bounds-error--sequence condition)))))

(define-condition sequence-index-error (sequence-out-of-bounds-error)
  ((sequence
    :initarg :sequence
    :initform (required-argument)
    :reader sequence-out-of-bounds-error--sequence
    :type sequence)
   (index
    :initarg :index
    :initform (required-argument)
    :reader sequence-out-of-bounds-error--index
    :type sequence-index))
  (:report
   (lambda (condition stream)
     (format stream "~@<The index ~D is out of bounds for ~
                        ~:_a sequence of length ~D: ~
                          ~4I~:_~S~:>"
             (sequence-out-of-bounds-error--index condition)
             (length (sequence-out-of-bounds-error--sequence condition))
             (sequence-out-of-bounds-error--sequence condition)))))

(define-api validate-sequence-bounds
    (sequence start end &optional length)
    (sequence sequence-index
              (or null sequence-length)
              &optional (or null sequence-length)
      => sequence sequence-index sequence-length sequence-length)
  "Signals an error of type SEQUENCE-BOUNDING-INDICES-ERROR if `start', and
`end' are not valid /bounding indices/ for sequence `sequence'.

If `start' is NIL, the bounds are assumed to start at the first element of
`sequence'.

If `end' is NIL, the bounds are assumed to end at the length of `sequence'.

If `length' is NIL, the length of the sequence is computed as by
LENGTH. \(The purpose of this parameter is to avoid having to compute the
length twice if you already had to do so.\)

This function returns `sequence', and the possibly updated values of
`start', `end', and `length'."
  (let* ((length (or length (length sequence)))
         (end    (or end    length)))
    (if (<= 0 start end length)
        (values sequence start end length)
        (error 'sequence-bounding-indices-error
               :sequence sequence :start start :end end))))

(defmacro check-sequence-bounds
    (seq-place start-place end-place &optional length-place &environment env)
  "This is a convenience wrapper around VALIDATE-SEQUENCE-BOUNDS to
automatically assign its return values to the specified places.

As the return type of VALIDATE-SEQUENCE-BOUND is known, your Common
Lisp implementation may henceforth be able to derive the types of the
places."
  (multiple-value-bind (vars vals storevars setter getter)
      (get-setf-expansion `(values ,seq-place
                                   ,start-place
                                   ,end-place
                                   ,@(when length-place
                                       (list length-place)))
                          env)
    `(let ,(mapcar #'list vars vals)
       (multiple-value-bind (,@storevars)
           (multiple-value-call #'validate-sequence-bounds ,getter)
         ,setter))))

(declaim (inline canonicalize-key))
(define-api canonicalize-key
    (key-designator)
    ((or null symbol function) => function)
  "Canonicalizes `key-designator' to a function object.
If `key-designator' is NIL, the IDENTITY function is returned."
  (if key-designator (ensure-function key-designator) #'identity))

(declaim (inline canonicalize-test))
(define-api canonicalize-test
    (test &optional test-not)
    ((or null symbol function) &optional (or null symbol function) => function)
  "Canonicalizes `test' and `test-not' to a function object.
If both are given, an error is signaled.
If both are NIL, the EQL function is returned."
  (cond ((and test test-not)
         (error "Both :TEST and :TEST-NOT passed."))
        (test     (ensure-function test))
        (test-not (ensure-function test-not))
        (t #'eql)))

;;; This was adapted from Christophe Rhodes' SB-SEQUENCES which were
;;; released into Public Domain.

(define-api make-sequence-like
    (sequence length &rest keys &key (initial-element nil iep) (initial-contents nil icp))
    (sequence sequence-length &key (:initial-element t) (:initial-contents sequence)
      => sequence)
  "Returns a new sequence of length `length' and of the same type as `sequence'.

The parameters `initial-element' and `initial-contents' specify how the new
sequence is supposed to be initialized. Only one of them can be passed at a
time.
"
  (declare (ignorable keys initial-element iep initial-contents icp))
  #+sbcl
  (apply #'sb-sequence:make-sequence-like sequence length keys)
  #-sbcl
  (etypecase sequence
    (list
       (cond
         ((and icp iep)
          (error "Both :INITIAL-ELEMENT and :INITIAL-CONTENTS passed."))
         (iep
          (make-list length :initial-element initial-element))
         (icp
          (assert (= (length initial-contents) length))
          (let ((result (make-list length)))
            (replace result initial-contents)
            result))
         (t
          (make-list length))))
    (vector
       (cond
         ((and icp iep)
          (error "Both :INITIAL-ELEMENT and :INITIAL-CONTENTS passed."))
         (iep
          (make-array length
                      :element-type (array-element-type sequence)
                      :initial-element initial-element))
         (icp
          (make-array length
                      :element-type (array-element-type sequence)
                      :initial-contents initial-contents))
         (t
          (make-array length
                      :element-type (array-element-type sequence)))))))


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %normalize-dosequence-binding-arg (arg)
    ;; ARG may be either ELT, or (ELT IDX).
    (if (consp arg)
        (flet ((length=2 (list) (and (cdr list) (null (cddr list)))))
          (assert (length=2 arg))       ; nicer error message.
          (values (first arg) (second arg)))
        (values arg (gensym "IDX+")))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %parse-dosequences-clauses (clauses)
    ;; CLAUSES are of the form
    ;;   ((ELT IDX) SEQUENCE RESULT :START s :END e :FROM-END t :PLACE name)
    (loop for (var seq result . rest) in clauses
          collect seq into seqs
          collect var into vars
          collect result into results
          collect rest into argss
          finally (return (values vars seqs results argss)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %parse-do-sequence-iterators-clauses (clauses)
    ;; CLAUSES are of the form
    ;;   ((ELT IDX) ITERATOR RESULT)
    (loop for (var-clause iterator result) in clauses
          for (var idx) = (multiple-value-list
                           (%normalize-dosequence-binding-arg var-clause))
          collect iterator into iterators
          collect var into vars
          collect idx into idxs
          collect result into results
          finally (return (values vars idxs iterators results)))))


(defmacro dosequence ((var sequence &optional result &key start end from-end place)
                      &body body &environment macro-env)
  "DOSEQUENCE iterates over the elements of `sequence' with `var'
bound to each element in turn, starting from `start' until `end'; if
`from-end' is T, the iteration is performed in reverse order.

Instead of a symbol, `var' can be a list of two elements (`elt'
`index') where `elt' is bound to the current element, and `index' to
its index into the sequence.

An implicit block named NIL surrounds DOSEQUENCE, hence RETURN may be
used to terminate the loop immediately. Otherwise `result' is returned
at the end of the loop.

`body' is executed in an implicit TAGBODY.

`result' is processed at a time where `var' \(or `elt'\) is bound to
the last element, and `index' \(if given\) to the index of the last
element.

If `place' is given, a /generalized reference/ for `place' is defined
within the lexical scope of `body' such that a) an invocation of (`place')
will \(re\)access the current element in the sequence, and such that b) an
invocation of \(SETF \(`place'\) `new-value'\) will destructively set the
current element in `sequence' to `new-value'. It is unspecified whether the
SETF function updates `var' (or `elt', respectively.) Beyond that, CLHS 3.6
applies.

The bounding indices `start' and `end' are _not_ checked for validity.

It is unspecified whether DOSEQUENCE establishes a new binding for
`var' on each iteration, or only establishes one initial binding and
assigns to it on subsequent iterations."
  (let ((it (gensym "ITERATOR+")))
    (multiple-value-bind (real-body decls)
        (tcr.parse-declarations-1.0::parse-body body :documentation nil)
      (let ((decl-env (parse-declarations decls macro-env)))
        `(with-sequence-iterator (,it ,sequence :start ,start :end ,end
                                      :from-end ,from-end
                                      :place ,place)
           ;; This assumes that nobody declares on #'NIL, which seems reasonable
           ;; Is it? Should I check for (NULL PLACE) before inserting declarations?
           ,@(build-declarations 'declare
               (filter-declaration-env decl-env :affecting `(#',place)))
           (do-sequence-iterator (,var ,it ,result)
             ,@(build-declarations 'declare
                 (filter-declaration-env decl-env :include :free)
                 (filter-declaration-env decl-env :include :bound
                                         :not-affecting `(#',place)))
             ,@real-body))))))

(defmacro dosequences* (((var sequence &optional result &key start end from-end place)
                         &rest more-clauses) &body body)
  "DOSEQUENCES* iterates through multiple sequences at once.

Each iterator is run sequentially until one exhausts; the `result'
form belonging to the clause of the iterator that exhausts first, is
returned.

For detailed information on the way the iterators are run, see
DO-SEQUENCE-ITERATORS*. For a description of the format of each
clause, see DOSEQUENCE. "
  (let* ((clauses (cons `(,var ,sequence ,result
                                 :start ,start :end ,end :from-end ,from-end
                                 :place ,place)
                        more-clauses))
         (iterators (make-gensym-list (length clauses) "ITERATOR+")))
    (multiple-value-bind (vars seqs results argss)
        (%parse-dosequences-clauses clauses)
      (with-sexp-builder (progn)
        (dolists ((iterator iterators) (seq seqs) (args argss))
          (nest `(with-sequence-iterator (,iterator ,seq ,@args))))
        (nest)
        (insert `(do-sequence-iterators*
                     ,(loop for var in vars
                            for iterator in iterators
                            for result in results
                            collect `(,var ,iterator ,result))
                   ,@body))))))


(defmacro do-sequence-iterator ((var iterator &optional result) &body body)
  "This is a convenience wrapper around DO-SEQUENCE-ITERATORS*."
  `(do-sequence-iterators* ((,var ,iterator ,result))
     ,@body))

(defmacro do-sequence-iterators* (((var iterator &optional result) &rest more-clauses)
                                 &body body)
    "DO-SEQUENCE-ITERATORS* advances multiple sequence iterators at
once. It relates to DO-SEQUENCE-ITERATORS as DO* relates to DO.

In each iteration, the iterators are run and checked for exhaustion
_sequentially_.

For each element produced by the currently executed iterator, the
respective `var' is bound to this element.

If no iterator exhausts, `body' will be executed.

If, however, an iterator exhausts, the iteration will terminate at
that point, and the `result' form of the clause denoting the currently
executed iterator is evaluated and returned. The form is evaluated in
an environment where all the `var's belonging to iterators already
executed are bound to the elements produced in the current iteration,
and the other variables are bound to the elements produced during the
previous iteration.

`body' is executed in an implicit TAGBODY, and the iteration is
performed in an implicit BLOCK named NIL."
  (let* ((clauses   (cons `(,var ,iterator ,result) more-clauses))
         (length    (length clauses))
         (iterators (make-gensym-list length "ITERATOR+"))
         (next?s    (make-gensym-list length "NEXT?+"))
         (loop-tag  (gensym "[DO-SEQUENCE-ITERATORS*]-LOOP+")))
    (multiple-value-bind (vars idxs iters results)
        (%parse-do-sequence-iterators-clauses clauses)
      `(prog ,(loop for var      in vars
                    for idx      in idxs
                    for iter     in iters
                    for iterator in iterators
                    for next?    in next?s
                    append `((,iterator ,iter) (,var) (,idx) (,next?)))
         ,loop-tag
         ,@(loop for iterator in iterators
                 for var      in vars
                 for idx      in idxs
                 for result   in results
                 for next?    in next?s
                 collecting
              (let ((tmpvar (gensym "TMP-VAR+"))
                    (tmpidx (gensym "TMP-IDX+")))
                `(let (,tmpvar ,tmpidx)
                   (multiple-value-setq (,next? ,tmpvar ,tmpidx) (funcall ,iterator))
                   ;; Stop as soon as the currently running iterator exhausts
                   (unless ,next? (return ,result))
                   (setq ,var ,tmpvar)
                   (setq ,idx ,tmpidx))))
          ,@body
          (go ,loop-tag)))))

(defmacro do-sequence-iterators (((var iterator &optional result) &rest more-clauses)
                                 &body body)
    "DO-SEQUENCE-ITERATORS advances multiple sequence iterators at
once.

In each iteration, all iterators are run in _parallel_.

If no iterator exhausts, the `var's, which may be of the form (`elt'
`idx'), will be bound to the elements produced, and `body' is executed.

If, however, an iterator exhausts, the `result' form of the clause
denoting the exhausted iterator will be evaluated (in an environment
where all the variables are still bound to the elements as produced in
the previous iteration), and returned. If several iterators exhaust,
each respective `result' form will be returned as multiple values (in
the order the respective clauses appeared.)

`body' is executed in an implicit TAGBODY, and the iteration is
performed in an implicit BLOCK named NIL."
  (let* ((clauses    (cons `(,var ,iterator ,result) more-clauses))
         (length     (length clauses))
         (iterators  (make-gensym-list length "ITERATOR+"))
         (tmp-vars   (make-gensym-list length "TMP-VAR+"))
         (tmp-idxs   (make-gensym-list length "TMP-IDX+"))
         (next?s     (make-gensym-list length "NEXT?+"))
         (loop-tag   (gensym "DOSEQUENCES-LOOP+")))
    (multiple-value-bind (vars idxs iters results)
        (%parse-do-sequence-iterators-clauses clauses)
      (with-sexp-builder (block nil)
        ;; Bind user's variables.
        (dolists ((var vars) (idx idxs) (iter iters) (iterator iterators))
          (nest `(let ((,iterator ,iter) (,var) (,idx)))))
        (nest)
        (insert `(tagbody ,loop-tag))
        ;; Run iterators; bind results to temporaries.
        (dolists ((iterator iterators) (tmp-var tmp-vars) (tmp-idx tmp-idxs) (next? next?s))
          (nest `(multiple-value-bind (,next? ,tmp-var ,tmp-idx)
                     (funcall ,iterator))))
        (nest)
        ;; Did an iterator run out of steam?
        (insert `(unless (and ,@next?s)
                   (return (values ,@results))))
        ;; Assign temporaries to user's variables.
        (dolists ((var vars) (idx idxs)
                  (tmp-var tmp-vars) (tmp-idx tmp-idxs))
          (insert `(setq ,var ,tmp-var))
          (insert `(setq ,idx ,tmp-idx)))
        ;; Execute body.
        (insert `(tagbody))
        (nest)
        (dolist (sexp body)
          (insert sexp))
        ;; Loop!
        (insert `(go ,loop-tag))))))

(defmacro with-sequence-iterator ((iterator-name sequence &key start end from-end place)
                                  &body body)
  "Within the lexical scope of `body', `iterator-name' is defined via FLET
such that each successive invocation of \(`iterator-name'\) will yield all
the elements of `sequence', one by one, starting from `start' until `end';
if `from-end' is T, in reverse order.

The /bounding indices/ `start', and `end' are _not_ checked for
validity upfront; to achieve that, use CHECK-SEQUENCE-BOUNDS.

Each invocation of the iterator form returns the following three values:

  1. A generalized boolean that is true if an element is returned.

  2. The element itself.

  3. The index of the element in the sequence.

If `place' is given, a /generalized reference/ for `place' is defined within
the lexical scope of `body' such that a) an invocation of (`place') will
\(re\)access the current element in the sequence, and such that b) an
invocation of \(SETF \(`place'\) `new-value'\) will destructively set the
current element in `sequence' to `new-value'. Beyond that, CLHS 3.6
applies.

Consequences are undefined if `place' is referenced or assigned to before
`iterator' has run, or after `iterator' has exhausted.

Consequences are undefined if a closure that closes over the iterator
form (or `place') is executed outside the dynamic extent of
WITH-SEQUENCE-ITERATOR.

Consequences are undefined if multiple threads invoke the iterator
concurrently."
  (let ((giterator (gensym "ITERATOR+"))
        (ggetter   (gensym "GETTER+"))
        (gsetter   (gensym "SETTER+")))
    ;; Yuck all these IGNORABLE declarations.
    `(multiple-value-bind (,giterator ,ggetter ,gsetter)
         (%make-sequence-iterator ,sequence :start ,start :end ,end
                                  :from-end ,from-end)
       (declare (ignorable ,giterator ,gsetter ,ggetter))
       ;;; FIXME: SBCL emits annoying notes
       #-sbcl (declare (dynamic-extent ,giterator ,gsetter ,ggetter))
       ,(if place
            `(flet ((,iterator-name ()
                      (funcall ,giterator))
                    (,place ()
                      (funcall ,ggetter))
                    ((setf ,place) (new-value)
                      (funcall ,gsetter new-value)))
               (declare (ignorable #',iterator-name #'(setf ,place) #',place))
               ;;; FIXME: trigger failed aver in SBCL
               #-sbcl
               (declare (inline ,iterator-name ,place (setf ,place)))
               (let ((,iterator-name #',iterator-name))
                 (declare (ignorable ,iterator-name))
                 ,@body))
            `(flet ((,iterator-name ()
                      (funcall ,giterator)))
               (declare (ignorable #',iterator-name))
               (let ((,iterator-name #',iterator-name))
                 (declare (ignorable ,iterator-name))
                   ,@body))))))

(declaim (ftype (function (list
                           (or null sequence-index)
                           (or null sequence-length))
                   (values list sequence-length &optional))
                reversed-list-of-ptrs))
(defun reversed-list-of-ptrs (list start end)
  ;; This is an optimized version of
  ;;   (LET ((LIST (SUBSEQ LIST START END)))
  ;;     (VALUES (NREVERSE (MAPLIST #'IDENTITY LIST))
  ;;             (LENGTH LIST))
  (declare (optimize (speed 3) (safety 1)))
  (let ((result nil)
        (start (or start 0))
        (endp (null end)))
    (declare (sequence-index start))
    (loop for sublist on (nthcdr start list)
          while (or endp (< start (the sequence-index end)))
          do
            (push sublist result)
            (incf start)
          finally
            (return (values result start)))))

;; %MAKE-SEQUENCE-ITERATOR (and not MAKE-SEQUENCE-ITERATOR) to clarify that
;; this is an internal function that is _not_ supposed to be exported. The
;; reason behind that is that Christopher Rhodes' proposal for extensible
;; sequences defines a MAKE-SEQUENCE-ITERATOR function that is quite
;; different to this one.
;;
(declaim (ftype (function (&rest t)
                  (values (function ()  (values boolean t sequence-index &optional))
                          (function ()  (values t &optional))
                          (function (t) (values t &optional))
                          &optional))
                %make-sequence-iterator))
(defun %make-sequence-iterator (sequence &key start end from-end)
  "Returns an iterator thunk that returns three values: a boolean flag
that is true, if an element is returned, the current sequence index,
and the current sequence element."
  (declare (optimize (debug 2)))
  (macrolet ((iterator-thunk (&key endp index elt step)
               `#'(lambda ()
                    (if ,endp
                        (values nil nil nil)
                        (multiple-value-prog1 (values t ,elt ,index)
                          ,step))))
             (reader-thunk (&key endp elt)
               `(locally (declare #+sbcl (sb-ext:muffle-conditions
                                          sb-ext:code-deletion-note))
                  #'(lambda ()
                      (if ,endp
                          (error "Iterator exhausted")
                          ,elt))))
             (writer-thunk (&key endp set type)
               `(locally (declare #+sbcl (sb-ext:muffle-conditions
                                          sb-ext:code-deletion-note))
                  #'(lambda (new-elt)
                      ,(when type
                         `(check-type new-elt ,type))
                      (if ,endp
                          (error "Iterator exhausted.")
                          (setf ,set new-elt)))))
             (make-random-access-iterator (sequence start end from-end &key accessor
                                                    element-type)
               (let ((accessor     (second accessor)) ; strip off QUOTE.
                     (element-type (second element-type)))
                 `(let ((seq ,sequence)
                        (start (or ,start 0))
                        (end   (or ,end (length sequence))))
                    (declare (type array-index start)
                             (type array-length end))
                    ;; We have to resort to :ENDP NIL (tantamount with not
                    ;; checking for exhaustion) sometimes because we'd
                    ;; otherwise reach out of the declared
                    ;; boundaries. Notice that WITH-SEQUENCE-ITERATOR is
                    ;; specified such that these cases invoke undefined
                    ;; behaviour.
                    (if ,from-end
                        (values (iterator-thunk
                                 :endp  (= end start)
                                 :index (1- end)
                                 :elt   (,accessor seq (1- end))
                                 :step  (decf end))
                                (reader-thunk
                                 :endp  nil
                                 :elt   (,accessor seq end))
                                (writer-thunk
                                 :endp  nil
                                 :set   (,accessor seq end)
                                 :type  ,element-type))
                        (let ((index start))
                          (declare (type array-length index))
                          (values (iterator-thunk
                                   :endp  (= index end)
                                   :index index
                                   :elt   (,accessor seq index)
                                   :step  (incf index))
                                  (reader-thunk
                                   :endp  (= index end)
                                   :elt   (,accessor seq (1- index)))
                                  (writer-thunk
                                   :endp  nil
                                   :set   (,accessor seq (1- index))
                                   :type  ,element-type)))))))
             (make-sequential-iterator (sequence start end from-end)
               `(if ,from-end
                    (multiple-value-bind (ptrs end)
                        (reversed-list-of-ptrs ,sequence ,start ,end)
                      (declare (sequence-length end))
                      (let ((storage-ptr))
                        (values (iterator-thunk
                                 :endp  (null ptrs)
                                 :index (1- end)
                                 :elt   (car (first ptrs))
                                 :step  (progn (setq storage-ptr (first ptrs))
                                               (pop ptrs)
                                               (decf end)))
                                ;; (NULL STORAGE-PTR) will only guard
                                ;; against the invocation of (PLACE) before
                                ;; iterator run problem, not the invocation
                                ;; of (PLACE) after iterator exhausted
                                ;; problem.
                                (reader-thunk
                                 :endp  (null storage-ptr)
                                 :elt   (car storage-ptr))
                                (writer-thunk
                                 :endp  (null storage-ptr)
                                 :set   (car storage-ptr)))))
                    (let* ((index   (or start 0))
                           (sublist (nthcdr index sequence))
                           (storage-ptr))
                      (declare (type sequence-length index))
                      (values (if (null end)
                                  (iterator-thunk
                                   :endp  (null sublist)
                                   :index index
                                   :elt   (first sublist)
                                   :step  (progn (setq storage-ptr sublist)
                                                 (pop sublist)
                                                 (incf index)))
                                  (iterator-thunk
                                   :endp  (>= index (the sequence-length end))
                                   :index index
                                   :elt   (first sublist)
                                   :step  (progn (setq storage-ptr sublist)
                                                 (pop sublist)
                                                 (incf index))))
                              (reader-thunk
                               :endp  (null storage-ptr)
                               :elt   (car storage-ptr))
                              (writer-thunk
                               :endp (null storage-ptr)
                               :set  (car storage-ptr)))))))
    (etypecase sequence
      (list
         (locally (declare (optimize (speed 3) (safety 1)))
           (make-sequential-iterator sequence start end from-end)))
      (simple-vector
         (locally (declare (optimize (speed 3) (safety 1)))
           (make-random-access-iterator sequence start end from-end :accessor 'svref)))
      (simple-string
         (locally (declare (optimize (speed 3) (safety 1)))
           (make-random-access-iterator sequence start end from-end
                                        :accessor 'schar
                                        :element-type 'character)))
      (string
         (locally (declare (optimize (speed 3) (safety 1))
                           ;; Upgraded element type is not known at compile time. Bummer.
                           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
           (make-random-access-iterator sequence start end from-end
                                        :accessor 'char
                                        :element-type 'character)))
      (vector
         (locally (declare (optimize (speed 3) (safety 1))
                           ;; Upgraded element type is not known at compile time. Bummer.
                           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
           (make-random-access-iterator sequence start end from-end :accessor 'aref)))
      (sequence
         (make-random-access-iterator sequence start end from-end :accessor 'elt)))))
